BINARY_NAME = go-simple-migrate

.DEFAULT_GOAL := build

fmt:
	go fmt ./...
.PHONY:fmt

lint: fmt
	golint ./...
.PHONY:lint

vet: fmt
	go vet ./...
.PHONY:vet

build: vet
	go build -o bin/${BINARY_NAME} ./cmd/main.go
.PHONY:build

run: build
	./bin/${BINARY_NAME}
.PHONY:run

clean:
	go clean
	rm -rf ./bin
.PHONY: clean

test:
	go test -cover ./...
.PHONY: test