package main

import (
	"os"

	"gitlab.com/19seanak19/go-simple-migrate/internal/cli"
)

var inArgs = os.Args
var logger = &cli.CliLogger{}

func main() {
	args, err := cli.ParseArgs(inArgs, nil)
	if err != nil {
		return
	}

	cli, err := cli.NewCli(args, logger)
	if err != nil {
		return
	}

	cli.Run()
}
