package sqlite_test

import (
	"database/sql"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/19seanak19/go-simple-migrate/internal/database/basic"
	"gitlab.com/19seanak19/go-simple-migrate/internal/database/sqlite"
)

type testBasicDb struct {
	openCount          int
	driver             string
	conn               string
	openErr            error
	closeCount         int
	closeErr           error
	didGetAppliedTag   bool
	appliedTagQuery    string
	appliedTagErr      error
	execQueries        []string
	execErrCondition   string
	execErr            error
	beginTxCount       int
	beginTxErr         error
	commitTxCount      int
	commitTxErr        error
	rollbackTxCount    int
	rollbackTxErr      error
	execTxQueries      []string
	execTxErrCondition string
	execTxErr          error
}

func (tb *testBasicDb) Open(driver string, conn string) (*sql.DB, error) {
	tb.openCount += 1
	tb.driver = driver
	tb.conn = conn
	return &sql.DB{}, tb.openErr
}

func (tb *testBasicDb) Close(db *sql.DB) error {
	tb.closeCount += 1
	return tb.closeErr
}

func (tb *testBasicDb) AppliedTagForSet(db *sql.DB, query string, args ...any) (string, error) {
	tb.didGetAppliedTag = true
	return "tag", tb.appliedTagErr
}

func (tb *testBasicDb) Exec(db *sql.DB, query string, args ...any) error {
	tb.execQueries = append(tb.execQueries, query)
	if tb.execErrCondition == "" || tb.execErrCondition == query {
		return tb.execErr
	} else {
		return nil
	}
}

func (tb *testBasicDb) BeginTx(db *sql.DB) (*sql.Tx, error) {
	tb.beginTxCount += 1
	return &sql.Tx{}, tb.beginTxErr
}

func (tb *testBasicDb) CommitTx(tx *sql.Tx) error {
	tb.commitTxCount += 1
	return tb.commitTxErr
}

func (tb *testBasicDb) RollbackTx(tx *sql.Tx) error {
	tb.rollbackTxCount += 1
	return tb.rollbackTxErr
}

func (tb *testBasicDb) ExecTx(tx *sql.Tx, query string, args ...any) error {
	tb.execTxQueries = append(tb.execTxQueries, query)
	if tb.execTxErrCondition == "" || tb.execTxErrCondition == query {
		return tb.execTxErr
	} else {
		return nil
	}
}

func Test_New(t *testing.T) {
	sl := sqlite.New()

	assert.NotNil(t, sl)
	assert.NotNil(t, sl.BasicDb)
	assert.IsType(t, &basic.Basic{}, sl.BasicDb)
}

func Test_Open_Success(t *testing.T) {
	tb := &testBasicDb{}
	sl := &sqlite.Sqlite{
		BasicDb: tb,
	}
	testConn := "test conn string"

	err := sl.Open(testConn)

	assert.Nil(t, err)
	assert.Equal(t, 1, tb.openCount)
	assert.Equal(t, "sqlite3", tb.driver)
	assert.Equal(t, testConn, tb.conn)
}

func Test_Open_Error(t *testing.T) {
	tb := &testBasicDb{
		openErr: errors.New("test error"),
	}
	sl := &sqlite.Sqlite{
		BasicDb: tb,
	}
	testConn := "test conn string"

	err := sl.Open(testConn)

	assert.NotNil(t, err)
	assert.Equal(t, tb.openErr, err)
	assert.Equal(t, 1, tb.openCount)
}

func Test_Close_Success(t *testing.T) {
	tb := &testBasicDb{}
	sl := &sqlite.Sqlite{
		BasicDb: tb,
	}

	err := sl.Close()

	assert.Nil(t, err)
	assert.Equal(t, 1, tb.closeCount)
}

func Test_Close_Error(t *testing.T) {
	tb := &testBasicDb{
		closeErr: errors.New("test error"),
	}
	sl := &sqlite.Sqlite{
		BasicDb: tb,
	}

	err := sl.Close()

	assert.NotNil(t, err)
	assert.Equal(t, tb.closeErr, err)
	assert.Equal(t, 1, tb.closeCount)
}

func Test_AppliedTagForSet_Success(t *testing.T) {
	tb := &testBasicDb{}
	sl := &sqlite.Sqlite{
		BasicDb: tb,
	}

	tag, err := sl.AppliedTagForSet("test set name")

	assert.Nil(t, err)
	assert.Equal(t, "tag", tag)
	assert.True(t, tb.didGetAppliedTag)
}

func Test_AppliedTagForSet_Error(t *testing.T) {
	tb := &testBasicDb{
		closeErr: errors.New("test error"),
	}
	sl := &sqlite.Sqlite{
		BasicDb: tb,
	}

	err := sl.Close()

	assert.NotNil(t, err)
	assert.Equal(t, tb.closeErr, err)
	assert.Equal(t, 1, tb.closeCount)
}

func Test_ApplyMigration_Success(t *testing.T) {
	tb := &testBasicDb{}
	sl := &sqlite.Sqlite{
		BasicDb: tb,
	}
	statement := "test migration statement"
	tag := "test tag"
	setname := "test set name"

	err := sl.ApplyMigration(statement, tag, setname)

	assert.Nil(t, err)
	assert.Equal(t, 1, tb.beginTxCount)
	assert.Equal(t, 1, tb.commitTxCount)
	assert.Equal(t, 0, tb.rollbackTxCount)
	assert.Len(t, tb.execTxQueries, 2)
	assert.Equal(t, statement, tb.execTxQueries[0])
	assert.Equal(t, sqlite.QueryUpsertTagForSet, tb.execTxQueries[1])
}

func Test_ApplyMigration_ErrorBeginTx(t *testing.T) {
	tb := &testBasicDb{
		beginTxErr: errors.New("Test error"),
	}
	sl := &sqlite.Sqlite{
		BasicDb: tb,
	}
	statement := "test migration statement"
	tag := "test tag"
	setname := "test set name"

	err := sl.ApplyMigration(statement, tag, setname)

	assert.NotNil(t, err)
	assert.Equal(t, tb.beginTxErr, err)
	assert.Equal(t, 1, tb.beginTxCount)
	assert.Equal(t, 0, tb.commitTxCount)
	assert.Equal(t, 0, tb.rollbackTxCount)
	assert.Len(t, tb.execTxQueries, 0)
}

func Test_ApplyMigration_ErrorStatement(t *testing.T) {
	tb := &testBasicDb{
		execTxErr: errors.New("Test error"),
	}
	sl := &sqlite.Sqlite{
		BasicDb: tb,
	}
	statement := "test migration statement"
	tag := "test tag"
	setname := "test set name"

	err := sl.ApplyMigration(statement, tag, setname)

	assert.NotNil(t, err)
	assert.Equal(t, tb.execTxErr, err)
	assert.Equal(t, 1, tb.beginTxCount)
	assert.Equal(t, 0, tb.commitTxCount)
	assert.Equal(t, 1, tb.rollbackTxCount)
	assert.Len(t, tb.execTxQueries, 1)
}

func Test_ApplyMigration_ErrorTagUpsert(t *testing.T) {
	tb := &testBasicDb{
		execTxErr:          errors.New("Test error"),
		execTxErrCondition: sqlite.QueryUpsertTagForSet,
	}
	sl := &sqlite.Sqlite{
		BasicDb: tb,
	}
	statement := "test migration statement"
	tag := "test tag"
	setname := "test set name"

	err := sl.ApplyMigration(statement, tag, setname)

	assert.NotNil(t, err)
	assert.Equal(t, tb.execTxErr, err)
	assert.Equal(t, 1, tb.beginTxCount)
	assert.Equal(t, 0, tb.commitTxCount)
	assert.Equal(t, 1, tb.rollbackTxCount)
	assert.Len(t, tb.execTxQueries, 2)
}

func Test_ApplyMigration_ErrorCommitTx(t *testing.T) {
	tb := &testBasicDb{
		commitTxErr: errors.New("Test error"),
	}
	sl := &sqlite.Sqlite{
		BasicDb: tb,
	}
	statement := "test migration statement"
	tag := "test tag"
	setname := "test set name"

	err := sl.ApplyMigration(statement, tag, setname)

	assert.NotNil(t, err)
	assert.Equal(t, tb.commitTxErr, err)
	assert.Equal(t, 1, tb.beginTxCount)
	assert.Equal(t, 1, tb.commitTxCount)
	assert.Equal(t, 1, tb.rollbackTxCount)
	assert.Len(t, tb.execTxQueries, 2)
}
