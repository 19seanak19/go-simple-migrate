// Package sqllite implements a DatabaseInterface for go-simple-migrate to apply migrations to sqlite databases.
package sqlite

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/19seanak19/go-simple-migrate/internal/database/basic"
)

const (
	// Name of the table for tracking applied tags.
	TagTable = "go_simple_migrate_tags"
	// Calmun in the tag table for set names.
	NameColumn = "name"
	// Column in the tag table for applied tags.
	TagColumn = "tag"
)

const (
	// SQL query to create the table for tracking applied tags.
	QueryCreateTagTable = "CREATE TABLE IF NOT EXISTS " + TagTable + " (" + NameColumn + " TEXT, " + TagColumn + " TEXT) PRIMARY KEY (" + NameColumn + ", " + TagColumn + ");"
	// SQL query for getting the currently applied tag for a set.
	QuerySelectTagForSet = "SELECT " + TagColumn + " FROM " + TagTable + " WHERE " + NameColumn + "=$1;"
	// SQL query for upserting an applied tag into the tracking table.
	QueryUpsertTagForSet = "INSERT INTO " + TagTable + " (" + NameColumn + ", " + TagColumn + ") VALUES ($1, $2) ON CONFLICT (" + NameColumn + ") UPDATE SET " + TagColumn + " = $2;"
)

// Type for communicating with sqllite databases to apply migrations.
type Sqlite struct {
	Db      *sql.DB
	BasicDb basic.BasicInterface
}

func New() *Sqlite {
	return &Sqlite{
		BasicDb: &basic.Basic{},
	}
}

// Opens a connection to the DB using the given connection string.
func (s *Sqlite) Open(conn string) error {
	db, err := s.BasicDb.Open("sqlite3", conn)
	if err != nil {
		return err
	}

	s.Db = db

	err = s.BasicDb.Exec(s.Db, QueryCreateTagTable)
	return err
}

// Closes any open connection to the DB.
func (s *Sqlite) Close() error {
	return s.BasicDb.Close(s.Db)
}

// Returns the currently applied tag for the set with the given name.
func (s *Sqlite) AppliedTagForSet(setName string) (string, error) {
	return s.BasicDb.AppliedTagForSet(s.Db, QuerySelectTagForSet, setName)
}

// Applies the given statement, updating the applied tag for the set name.
// The tag for Ups should be the statement's tag.
// The tag for Downs should be the previous statement's tag.
func (s *Sqlite) ApplyMigration(statement string, tag string, setName string) error {
	tx, err := s.BasicDb.BeginTx(s.Db)
	if err != nil {
		return err
	}

	err = s.BasicDb.ExecTx(tx, statement)
	if err != nil {
		s.BasicDb.RollbackTx(tx)
		return err
	}

	err = s.BasicDb.ExecTx(tx, QueryUpsertTagForSet, setName, tag)
	if err != nil {
		s.BasicDb.RollbackTx(tx)
		return err
	}

	err = s.BasicDb.CommitTx(tx)
	if err != nil {
		s.BasicDb.RollbackTx(tx)
	}
	return err
}
