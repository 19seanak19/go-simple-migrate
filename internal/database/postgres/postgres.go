// Package postgres implements a DatabaseInterface for go-simple-migrate to apply migrations to postgres databases.
package postgres

import (
	"context"

	"github.com/jackc/pgx/v4"
)

const (
	// Name of the table for tracking applied tags.
	tagTable = "go_simple_migrate_tags"
	// Calmun in the tag table for set names.
	nameColumn = "name"
	// Column in the tag table for applied tags.
	tagColumn = "tag"
)

const (
	// SQL query to create the table for tracking applied tags.
	queryCreateTagTable = "CREATE TABLE IF NOT EXISTS " + tagTable + " (" + nameColumn + " TEXT, " + tagColumn + " TEXT) PRIMARY KEY (" + nameColumn + ", " + tagColumn + ");"
	// SQL query for getting the currently applied tag for a set.
	querySelectTagForSet = "SELECT " + tagColumn + " FROM " + tagTable + " WHERE " + nameColumn + "=$1;"
	// SQL query for upserting an applied tag into the tracking table.
	queryUpsertTagForSet = "INSERT INTO " + tagTable + " (" + nameColumn + ", " + tagColumn + ") VALUES ($1, $2) ON CONFLICT (" + nameColumn + ") UPDATE SET " + tagColumn + " = $2;"
)

// Type for communicating with Postgres databases to apply migrations.
type Postgres struct {
	db *pgx.Conn
}

// Opens a connection to the Postgres DB at the given connection string.
func (p *Postgres) Open(conn string) error {
	db, err := pgx.Connect(context.Background(), conn)
	if err != nil {
		return err
	}

	p.db = db

	_, err = p.db.Exec(context.Background(), queryCreateTagTable)

	return err
}

// Closes the open connection if present.
func (p *Postgres) Close() error {
	if p.db == nil {
		return nil
	}

	return p.db.Close(context.Background())
}

// Returns the currently applied tag for the set with the given name.
func (p *Postgres) AppliedTagForSet(setName string) (string, error) {
	rows, err := p.db.Query(context.Background(), querySelectTagForSet, setName)
	if err != nil {
		return "", err
	}
	defer rows.Close()

	if !rows.Next() {
		return "", nil
	}

	var tag string
	err = rows.Scan(&tag)

	return tag, err
}

// Applies the given statement, then updates the applied tag for the given set name.
func (p *Postgres) ApplyMigration(statement string, tag string, setName string) error {
	_, err := p.db.Exec(context.Background(), statement)
	if err != nil {
		return err
	}

	_, err = p.db.Exec(context.Background(), queryUpsertTagForSet, setName, tag)
	if err != nil {
		return err
	}

	return nil
}
