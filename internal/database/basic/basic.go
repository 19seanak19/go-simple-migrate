// Package basic procides a default DatabaseInterface implementation for databases that use database/sql drivers.
package basic

import (
	"database/sql"
)

// Interface for types that implement default functions that can be used for implementing a DatabaseInterface using database/sql drivers.
type BasicInterface interface {
	// Opens a connection to the DB using the given connection string.
	Open(driver string, conn string) (*sql.DB, error)
	// Closes any open connection to the DB.
	Close(db *sql.DB) error
	// Returns the currently applied tag for the set with the given name.
	AppliedTagForSet(db *sql.DB, query string, args ...any) (string, error)
	// Executes the given statement, ignoring the result and returning any errors.
	Exec(db *sql.DB, query string, args ...any) error
	// Crates a new transaction.
	BeginTx(db *sql.DB) (*sql.Tx, error)
	// Commits the given transaction.
	CommitTx(tx *sql.Tx) error
	// Rollbacks the given transaction.
	RollbackTx(tx *sql.Tx) error
	// Executes the given statement using the transaction, ignoring the result and returning any errors.
	ExecTx(tx *sql.Tx, query string, args ...any) error
}

// Type that impleemts default functions that can be used for implementing a DatabaseInterface using database/sql drivers.
type Basic struct {
}

// Opens a connection to the DB using the given connection string.
func (b *Basic) Open(driver string, conn string) (*sql.DB, error) {
	db, err := sql.Open(driver, conn)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}

// Closes any open connection to the DB.
func (b *Basic) Close(db *sql.DB) error {
	return db.Close()
}

// Returns the currently applied tag for the set with the given name.
func (b *Basic) AppliedTagForSet(db *sql.DB, query string, args ...any) (string, error) {
	rows, err := db.Query(query, args)
	if err != nil {
		return "", err
	}
	defer rows.Close()

	if !rows.Next() {
		return "", nil
	}

	var tag string
	err = rows.Scan(&tag)

	return tag, err
}

// Executes the given statement, ignoring the result and returning any errors.
func (b *Basic) Exec(db *sql.DB, query string, args ...any) error {
	_, err := db.Exec(query, args)
	return err
}

// Crates a new transaction.
func (b *Basic) BeginTx(db *sql.DB) (*sql.Tx, error) {
	return db.Begin()
}

// Commits the given transaction.
func (b *Basic) CommitTx(tx *sql.Tx) error {
	return tx.Commit()
}

// Rollbacks the given transaction.
func (b *Basic) RollbackTx(tx *sql.Tx) error {
	return tx.Rollback()
}

// Executes the given statement using the transaction, ignoring the result and returning any errors.
func (b *Basic) ExecTx(tx *sql.Tx, query string, args ...any) error {
	_, err := tx.Exec(query, args...)
	return err
}
