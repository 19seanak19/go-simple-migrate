package cli_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/19seanak19/go-simple-migrate/internal/cli"
	"gitlab.com/19seanak19/go-simple-migrate/pkg/migrate"
)

func Test_ParseArgs_Success(t *testing.T) {
	confPath := "path/to/conf.json"
	dbType := "type"
	dbConn := "connString"
	migDir := "path/to/dir"
	logLevel := "LEVEL"
	inArgs := []string{
		"progName",
		"-configPath", confPath,
		"-dbType", dbType,
		"-dbConn", dbConn,
		"-migrationsDir", migDir,
		"-logLevel", logLevel,
	}

	expectedArgs := &cli.Args{
		ConfigPath:    confPath,
		DbType:        dbType,
		DbConn:        dbConn,
		MigrationsDir: migDir,
		LogLevel:      logLevel,
	}

	args, err := cli.ParseArgs(inArgs, &bytes.Buffer{})

	require.Nil(t, err)
	assert.Equal(t, expectedArgs, args)
}

func Test_ParseArgs_Error(t *testing.T) {
	inArgs := []string{
		"progName",
		"-unknownFlag", "SomeValue",
	}

	_, err := cli.ParseArgs(inArgs, &bytes.Buffer{})

	require.NotNil(t, err)
}

func Test_ConfigFromArgs(t *testing.T) {
	args := &cli.Args{
		DbType:        "postgres",
		DbConn:        "connection_string",
		MigrationsDir: "migrationsDir",
	}

	conf := cli.ConfigFromArgs(args)

	assert.Equal(t, args.DbType, conf.DbType)
	assert.Equal(t, args.DbConn, conf.DbConn)
	assert.Equal(t, args.MigrationsDir, conf.MigrationsDir)
}

func Test_OverrideConfig(t *testing.T) {
	conf := &migrate.Config{
		DbType:        "oldType",
		DbConn:        "oldConn",
		MigrationsDir: "oldDir",
	}
	args := &cli.Args{
		DbType:        "newType",
		DbConn:        "newConn",
		MigrationsDir: "newDir",
	}

	args.OverrideConfig(conf)

	assert.Equal(t, args.DbType, conf.DbType)
	assert.Equal(t, args.DbConn, conf.DbConn)
	assert.Equal(t, args.MigrationsDir, conf.MigrationsDir)
}
