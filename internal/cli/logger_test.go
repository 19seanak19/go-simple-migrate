package cli_test

import (
	"testing"

	"github.com/sirupsen/logrus"
	logrusTest "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/19seanak19/go-simple-migrate/internal/cli"
)

const testMsg = "This is a test log message."

func testLoggerAndHook() (*cli.CliLogger, *logrusTest.Hook) {
	nullLogger, hook := logrusTest.NewNullLogger()
	logger := cli.NewCliLogger()
	logger.Logger = nullLogger
	logger.SetLogLevel(cli.TraceLogLevel)
	return logger, hook
}

func Test_Trace(t *testing.T) {
	logger, hook := testLoggerAndHook()

	logger.Trace(testMsg)

	require.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.TraceLevel, hook.LastEntry().Level)
	assert.Equal(t, testMsg, hook.LastEntry().Message)
	hook.Reset()
}

func Test_Debug(t *testing.T) {
	logger, hook := testLoggerAndHook()

	logger.Debug(testMsg)

	require.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.DebugLevel, hook.LastEntry().Level)
	assert.Equal(t, testMsg, hook.LastEntry().Message)
	hook.Reset()
}

func Test_Info(t *testing.T) {
	logger, hook := testLoggerAndHook()

	logger.Info(testMsg)

	require.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.InfoLevel, hook.LastEntry().Level)
	assert.Equal(t, testMsg, hook.LastEntry().Message)
	hook.Reset()
}

func Test_Warn(t *testing.T) {
	logger, hook := testLoggerAndHook()

	logger.Warn(testMsg)

	require.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.WarnLevel, hook.LastEntry().Level)
	assert.Equal(t, testMsg, hook.LastEntry().Message)
	hook.Reset()
}

func Test_Error(t *testing.T) {
	logger, hook := testLoggerAndHook()

	logger.Error(testMsg)

	require.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.ErrorLevel, hook.LastEntry().Level)
	assert.Equal(t, testMsg, hook.LastEntry().Message)
	hook.Reset()
}

func Test_SetLevel(t *testing.T) {
	logger := cli.NewCliLogger()

	logger.SetLogLevel(cli.TraceLogLevel)
	assert.Equal(t, logrus.TraceLevel, logger.Logger.Level)

	logger.SetLogLevel(cli.DebugLogLevel)
	assert.Equal(t, logrus.DebugLevel, logger.Logger.Level)

	logger.SetLogLevel(cli.InfoLogLevel)
	assert.Equal(t, logrus.InfoLevel, logger.Logger.Level)

	logger.SetLogLevel(cli.WarnLogLevel)
	assert.Equal(t, logrus.WarnLevel, logger.Logger.Level)

	logger.SetLogLevel(cli.ErrorLogLevel)
	assert.Equal(t, logrus.ErrorLevel, logger.Logger.Level)

	logger.SetLogLevel(cli.NoneLogLevel)
	assert.Equal(t, logrus.FatalLevel, logger.Logger.Level)
}
