// Package cli implements functions and utilities for running the CLI tool for go-simple-migrate.
package cli

import (
	"fmt"

	"gitlab.com/19seanak19/go-simple-migrate/pkg/migrate"
)

type CliInterface interface {
	// Runs the default command for the CLI, applying migrations as defined in the args/config.
	Run()
}

// Type that implements all functionality of the CLI tool.
type Cli struct {
	// Args passed to the tool.
	args *Args
	// Config used by the tool. Either read from file or built from args.
	conf *migrate.Config
	// Logger used while running the tool.
	Logger *CliLogger
	// Runner used to complete migration tasks.
	Runner migrate.RunnerInterface
}

// Runs the default command for the CLI, applying migrations as defined in the args/config.
func (cli *Cli) Run() {
	err := cli.Runner.ApplyMigration()
	if err != nil {
		cli.Logger.Debug(fmt.Sprintf("Error details: %v", err))
		return
	}
}

func NewCli(args *Args, logger *CliLogger) (*Cli, error) {
	conf := ConfigFromArgs(args)

	if args.ConfigPath != "" {
		jsonConf, err := migrate.ConfigFromJson(args.ConfigPath)
		if err != nil {
			logger.Error(fmt.Sprintf("Failed to read config file at '%s'", args.ConfigPath))
			logger.Debug(fmt.Sprintf("Error details: %v", err))
			return nil, err
		}
		conf = jsonConf
		args.OverrideConfig(conf)
	}

	runner, err := migrate.NewRunner(conf, migrate.DefaultReader, migrate.DefaultMigrator, logger)
	if err != nil {
		logger.Error("Failed to read migration files")
		logger.Debug(fmt.Sprintf("Error details: %v", err))
		return nil, err
	}

	return &Cli{
		args:   args,
		conf:   conf,
		Logger: logger,
		Runner: runner,
	}, nil
}
