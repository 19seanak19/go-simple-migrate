package cli

import (
	"flag"
	"io"

	"gitlab.com/19seanak19/go-simple-migrate/pkg/migrate"
)

const (
	configPathFlag    = "configPath"
	configPathDesc    = "Path to the config file for the desired migrations"
	dbTypeFlag        = "dbType"
	dbTypeDesc        = "Type of the database (i.e. postgres, mysql, etc.), see docs for available types"
	dbConnFlag        = "dbConn"
	dbConnDesc        = "Connection string to use when connecting to the database"
	migrationsDirFlag = "migrationsDir"
	migrationsDirDesc = "Path to the directory containing the migration files"
	logLevelFlag      = "logLevel"
	logLevelDesc      = "Available levels: TRACE, DEBUG, INFO, WARN, ERROR."
)

type Args struct {
	// Path to the config file for running the tool.
	ConfigPath string
	// Type of the database (i.e. postgres, mysql, etc.). See docs for available types.
	DbType string
	// Connection string to use in order to connect to the database.
	DbConn string
	// Path to the directory containing the migration files.
	MigrationsDir string
	// Log level to disply when running migrations.
	LogLevel string
}

// Updates the given config by overriding values set in the Args.
func (a *Args) OverrideConfig(conf *migrate.Config) {
	if a.DbType != "" {
		conf.DbType = a.DbType
	}
	if a.DbConn != "" {
		conf.DbConn = a.DbConn
	}
	if a.MigrationsDir != "" {
		conf.MigrationsDir = a.MigrationsDir
	}
}

// Parses arg strings into Args. Input should be os.Args.
//
// Parameter output is used by the flag package. It should be nil in most cases.
func ParseArgs(args []string, output io.Writer) (*Args, error) {
	flags := flag.NewFlagSet(args[0], flag.ContinueOnError)

	flags.SetOutput(output)

	var a Args

	flags.StringVar(&a.ConfigPath, configPathFlag, "", configPathDesc)
	flags.StringVar(&a.DbType, dbTypeFlag, "", dbTypeDesc)
	flags.StringVar(&a.DbConn, dbConnFlag, "", dbConnDesc)
	flags.StringVar(&a.MigrationsDir, migrationsDirFlag, "", migrationsDirDesc)
	flags.StringVar(&a.LogLevel, logLevelFlag, InfoLogLevel, logLevelDesc)

	err := flags.Parse(args[1:])
	if err != nil {
		return nil, err
	}

	return &a, err
}

// Returns a basic config using the values in the given Args.
func ConfigFromArgs(args *Args) *migrate.Config {
	return &migrate.Config{
		DbType:        args.DbType,
		DbConn:        args.DbConn,
		MigrationsDir: args.MigrationsDir,
	}
}
