package cli

import (
	log "github.com/sirupsen/logrus"
)

// Log level identifier constants.
const (
	TraceLogLevel = "TRACE"
	DebugLogLevel = "DEBUG"
	InfoLogLevel  = "INFO"
	WarnLogLevel  = "WARN"
	ErrorLogLevel = "ERROR"
	NoneLogLevel  = "NONE"
)

// The Logger used when running the CLI.
type CliLogger struct {
	Logger *log.Logger
}

func (cl *CliLogger) Trace(msg string) {
	cl.Logger.Trace(msg)
}

func (cl *CliLogger) Debug(msg string) {
	cl.Logger.Debug(msg)
}

func (cl *CliLogger) Info(msg string) {
	cl.Logger.Info(msg)
}

func (cl *CliLogger) Warn(msg string) {
	cl.Logger.Warn(msg)
}

func (cl *CliLogger) Error(msg string) {
	cl.Logger.Error(msg)
}

// Sets the log level used by CliLogger.
func (cl *CliLogger) SetLogLevel(level string) {
	switch level {
	case TraceLogLevel:
		cl.Logger.SetLevel(log.TraceLevel)
	case DebugLogLevel:
		cl.Logger.SetLevel(log.DebugLevel)
	case InfoLogLevel:
		cl.Logger.SetLevel(log.InfoLevel)
	case WarnLogLevel:
		cl.Logger.SetLevel(log.WarnLevel)
	case ErrorLogLevel:
		cl.Logger.SetLevel(log.ErrorLevel)
	case NoneLogLevel:
		cl.Logger.SetLevel(log.FatalLevel)
	}
}

func NewCliLogger() *CliLogger {
	return &CliLogger{
		Logger: log.New(),
	}
}
