package cli_test

import (
	"errors"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/19seanak19/go-simple-migrate/internal/cli"
)

type testMigRunner struct {
	didApplyMigration bool
	applyMigrationErr error
}

func (tmr *testMigRunner) ApplyMigration() error {
	tmr.didApplyMigration = true
	return tmr.applyMigrationErr
}

func (tmr *testMigRunner) OrderMigrations() {

}

func Test_Run_Success(t *testing.T) {
	runner := &testMigRunner{}
	logger, hook := testLoggerAndHook()

	cli := &cli.Cli{
		Runner: runner,
		Logger: logger,
	}

	cli.Run()

	assert.True(t, runner.didApplyMigration)
	assert.Equal(t, 0, len(hook.Entries))
}

func Test_Run_Error(t *testing.T) {
	runner := &testMigRunner{}
	runner.applyMigrationErr = errors.New("Test error")
	logger, hook := testLoggerAndHook()

	cli := &cli.Cli{
		Runner: runner,
		Logger: logger,
	}

	cli.Run()

	assert.True(t, runner.didApplyMigration)
	require.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.DebugLevel, hook.LastEntry().Level)
}

func Test_NewCli_Success(t *testing.T) {
	args := &cli.Args{
		ConfigPath: "testdata/test_config.json",
	}
	logger, hook := testLoggerAndHook()

	cli, err := cli.NewCli(args, logger)

	assert.Nil(t, err)
	assert.NotNil(t, cli)
	assert.Equal(t, 0, len(hook.Entries))
}

func Test_NewCli_ErrorReadConfig(t *testing.T) {
	args := &cli.Args{
		ConfigPath: "testdata/test_config_missing.json",
	}
	logger, hook := testLoggerAndHook()

	cli, err := cli.NewCli(args, logger)

	assert.Nil(t, cli)
	assert.NotNil(t, err)
	require.Equal(t, 2, len(hook.Entries))
	assert.Equal(t, logrus.ErrorLevel, hook.Entries[0].Level)
	assert.Equal(t, logrus.DebugLevel, hook.Entries[1].Level)
}

func Test_NewCli_ErrorReadMigrations(t *testing.T) {
	args := &cli.Args{
		MigrationsDir: "testdata/nonexistant_dir",
	}
	logger, hook := testLoggerAndHook()

	cli, err := cli.NewCli(args, logger)

	assert.Nil(t, cli)
	assert.NotNil(t, err)
	require.Equal(t, 2, len(hook.Entries))
	assert.Equal(t, logrus.ErrorLevel, hook.Entries[0].Level)
	assert.Equal(t, logrus.DebugLevel, hook.Entries[1].Level)
}
