-- migration_set_a, any other comments can go here

-- up, 1, seankimball, any other comments can go here

CREATE TABLE examples (
    id INTEGER GENERATED ALWAYS AS IDENTITY,
    details VARCHAR(64),
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);

-- down, 1, seankimball

DROP TABLE examples;

-- up, 2, seankimball

ALTER TABLE examples
    ADD COLUMN edited_at TIMESTAMP WITH TIME ZONE;

-- down, 2, seankimball

ALTER TABLE examples 
    DROP COLUMN edited_at;

-- end