# go-simple-migrate

This is a simple migration package written in Go.


## Installation

`go get gitlab.com/19seanak19/go-simple-migrate`


## Usage

The package itself can be used from within another Go project, or via a CLI tool.

### Configuration

Using both the package and CLI requires some configuration in a JSON file. Possible contents of that file are below.

| Key | Type | Description |
| --- | --- | --- |
| dbType | `string` | Type of database. Currently just "postgres" is supported. |
| dbConn | `string` | Connection string to use when connecting to the database. |
| migrationsDir | `string` | Path to the directory containing the migration files. |
| order | `string[]` | Array of migration file names (name defined in first comment, not filename). The order here determines the order in which each file is applied. Any missing files are applied in alphabetical order after these. |
| desiredTags | `object` | Object mapping migration file names to the maximum Up statement tag to apply. Any missing files will apply all defined Up statements. |
| allowAutoDown | `boolean` | Determines whether or not the tool is allowed to automatically apply Down statements when the already applied Up tag is ordered after the desired tag. |

An example file can be found at `example/config.json`.

### Migration Files

For this tool to do anything, migration files need to be created. These files should be stored in a single directory. The files themselves should be
`.sql` files containing commented SQL statements. These SQL statements should define Ups (statements that apply a migration) and Downs (statements that
undo a migration).

The format of the file and comments must match the following guidelines:

- The first line of the file must be a comment naming the set of statements contained in the file. It should be formatted:
  ```sql
  -- set_name, other comments
  ```
  It is important to include the "," following set_name. Any other comments are allowed after the ",". The name itself also has no guidelines on formatting.

- Before each Up statement must be a comment marking it as an Up and tagging it. This should be formatted:
  ```sql
  -- up, tag, other comments
  ```
  Again, the "," after "up" and the tag must be included. After the tag's "," any other comments are allowed. The tag itself has no guidelines on formatting,
  though it's recommended to use incrementing numbers.

- The comment's preceding Down statements follow the same rules as Up statements, just replacing "up" with "down". The Down statement's tag should be the same
  as its matching Up statement. All Up statements must have a matching Down statement. Each Down statement should be defined directly after the matching Up.

- After the last statement in a file should be an end comment. This should be formatted:
  ```sql
  -- end
  ```

Example migration files can be found in `example/migrations`.

### Package

```go
import "gitlab.com/19seanak19/go-simple-migrate/pkg/migrate"

conf, err := migrate.ConfigFromJson("/path/to/config.json")
if err != nil {
  // Errors from reading config returned here.
}

runner, err := migrate.NewDefaultRunner(conf)
if err != nil {
    // Errors from reading migration files returned here.
}

err = runner.ApplyMigration()
if err != nil {
    // Errors from interacting with the DB returned here.
}
```

### CLI

`go-simple-migrate [flags]`

Available flags are described below. Any flags that conflict with config values will override those values when used.

| Flag | Description | Example |
| --- | --- | --- |
| -configPath | Path to a configuration JSON file. | `-configPath path/to/config.json` |
| -dbType | Type of database. Overrides the config's type. Supports the same values as the config. | `-dbType postgres` |
| -dbConn | Connection string to use in order to connect to the database. Overrides the config's connection string. | `-dbConn postgres://user:pass@host:5432/db_name` |
| -migrationsDir | Path to the directory containing migration files. | `-migrationsDir configs/migration.json` |
| -logLevel | Level of logs to display when running tool. Available levels: DEBUG, INFO, WARN, ERROR, NONE | `-logLevel WARN` |


## License

This package is licensed under the MIT License.
