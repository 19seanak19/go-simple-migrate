package files_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/19seanak19/go-simple-migrate/pkg/files"
)

const (
	testGoodMigrationsDir = "testdata"
	testNoneMigrationsDir = "testdata/test_migrations_not_here"
)

const (
	testSetNonePath = "testdata/set_none.sql"
	testSetAPath    = "testdata/set_a.sql"
	testSetBPath    = "testdata/set_b.sql"
)

var testMigration = &files.Migration{
	StatementSets: []files.StatementSet{
		testSetA,
		testSetB,
	},
}

var testSetA = files.StatementSet{
	FilePath: testSetAPath,
	Name:     "set_a",
	Statements: []files.Statement{
		{
			FilePath:  testSetAPath,
			Type:      files.UpType,
			Tag:       "1",
			StartLine: 4,
			EndLine:   6,
		},
		{
			FilePath:  testSetAPath,
			Type:      files.DownType,
			Tag:       "1",
			StartLine: 10,
			EndLine:   11,
		},
		{
			FilePath:  testSetAPath,
			Type:      files.UpType,
			Tag:       "2",
			StartLine: 15,
			EndLine:   16,
		},
		{
			FilePath:  testSetAPath,
			Type:      files.DownType,
			Tag:       "2",
			StartLine: 20,
			EndLine:   21,
		},
	},
}

var testSetB = files.StatementSet{
	FilePath: testSetBPath,
	Name:     "set_b",
	Statements: []files.Statement{
		{
			FilePath:  testSetBPath,
			Type:      files.UpType,
			Tag:       "1",
			StartLine: 4,
			EndLine:   6,
		},
		{
			FilePath:  testSetBPath,
			Type:      files.DownType,
			Tag:       "1",
			StartLine: 10,
			EndLine:   11,
		},
		{
			FilePath:  testSetBPath,
			Type:      files.UpType,
			Tag:       "2",
			StartLine: 15,
			EndLine:   16,
		},
		{
			FilePath:  testSetBPath,
			Type:      files.DownType,
			Tag:       "2",
			StartLine: 20,
			EndLine:   21,
		},
	},
}

func Test_MigrationFromDir_Success(t *testing.T) {
	reader := files.Reader{}

	migration, err := reader.MigrationFromDir(testGoodMigrationsDir)

	require.Nil(t, err)
	assert.Equal(t, testMigration, migration)
}

func Test_MigrationFromDir_ErrorMissingDir(t *testing.T) {
	reader := files.Reader{}

	_, err := reader.MigrationFromDir(testNoneMigrationsDir)

	require.NotNil(t, err)
}

func Test_StatementSetFromFile_Success(t *testing.T) {
	reader := files.Reader{}

	set, err := reader.StatementSetFromFile(testSetAPath)

	require.Nil(t, err)
	assert.Equal(t, &testSetA, set)
}

func Test_StatementSetFromFile_ErrorNotFound(t *testing.T) {
	reader := files.Reader{}

	_, err := reader.StatementSetFromFile(testSetNonePath)

	require.NotNil(t, err)
}

func Test_StatementText_Success(t *testing.T) {
	reader := files.Reader{}

	text, err := reader.StatementText(&testSetA.Statements[0])

	require.Nil(t, err)
	assert.Equal(t, text, "This is Up 1 line 1.\nThis is Up 1 line 2.\nThis is Up 1 line 3.")
}

func Test_StatementText_ErrorNotFound(t *testing.T) {
	reader := files.Reader{}
	missingStatement := &files.Statement{
		FilePath: testSetNonePath,
	}

	_, err := reader.StatementText(missingStatement)

	require.NotNil(t, err)
}
