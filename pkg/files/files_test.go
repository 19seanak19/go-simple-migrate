package files_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/19seanak19/go-simple-migrate/pkg/files"
)

func Test_UpTagOrder(t *testing.T) {
	ss := files.StatementSet{
		Statements: []files.Statement{
			{
				Type: files.UpType,
				Tag:  "One",
			},
			{
				Type: files.DownType,
				Tag:  "One",
			},
			{
				Type: files.UpType,
				Tag:  "Three",
			},
			{
				Type: files.DownType,
				Tag:  "Three",
			},
			{
				Type: files.UpType,
				Tag:  "Six",
			},
			{
				Type: files.DownType,
				Tag:  "Six",
			},
		},
	}
	expected := map[string]int{"One": 0, "Three": 2, "Six": 4}

	rawOrder := ss.UpTagOrder()
	order := map[string]int{}
	for tag, index := range rawOrder {
		order[tag] = *index
	}

	assert.Equal(t, order, expected)
}
