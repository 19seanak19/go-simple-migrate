// Package files defines types represented by the migration files, and utilities for reading those files.
//
// In general, this package should not be used directly.
package files

const (
	// Constant used in a Statement's Type field for Up statements.
	UpType = "up"
	// Constant used in a Statement's Type field for Down statements.
	DownType = "down"
)

// Represents a single SQL query in a migration, either an up or down.
type Statement struct {
	// Path to teh file that contains this statement.
	FilePath string
	// Type of the statement, either Up or Down
	Type string
	// Tag of the statement, the first field in the statement's comment afer Up or Down.
	Tag string
	// Line of the file this statement starts on (inclusive).
	StartLine int
	// Line of the file this statement ends on (inclusive).
	EndLine int
}

// Represents a single migration file.
type StatementSet struct {
	// Path to the file that holds the statements for this set.
	FilePath string
	// Name of the set, as defined on the first line of the set's file.
	Name string
	// All the statements in the set, ordered as defined in the set's file.
	Statements []Statement
}

// Returns a map of Up statement tags to an index of their order in the Statements list.
// Values closer to 0 are defined earlier in the Statements list (and earlier in the file).
func (ss *StatementSet) UpTagOrder() map[string]*int {
	order := map[string]*int{}

	for i, s := range ss.Statements {
		if s.Type == DownType {
			continue
		}
		index := new(int)
		*index = i
		order[s.Tag] = index
	}

	return order
}

// Represents all the migrations contained in a migration directory.
type Migration struct {
	// The list of statement sets.
	StatementSets []StatementSet
}
