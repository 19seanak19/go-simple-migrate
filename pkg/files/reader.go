package files

import (
	"bufio"
	"os"
	"strings"
)

const (
	// Extension used for migration files.
	migrationFileExt = ".sql"
	// Prefix indicating a comment in a migration file.
	commentPrefix = "--"
	// Separator that separates components of a migration comment.
	tagSeparator = ","
	// Tag in a comment indicating the end of a migration file.
	EndTag = "end"
)

type lineType int

const (
	// Comment line that contains the name of a statement set.
	lineCommentStatementSetName lineType = iota
	// Comment line that contains the tag for an Up statement.
	lineCommentStatementUpTag
	// Comment line that contains the tag for a Down statemet.
	lineCommentStatementDownTag
	// Comment line that indicates the end of a statement set.
	lineCommentStatementSetEnd
	// An empty line.
	lineEmpty
	// A line that is part of a statement.
	lineStatement
)

// Interface dfinition for types that can read migration files.
type ReaderInterface interface {
	MigrationFromDir(dirPath string) (*Migration, error)
	StatementSetFromFile(path string) (*StatementSet, error)
	StatementText(statement *Statement) (string, error)
}

// Type responsible for reading migration files.
type Reader struct {
}

// Parses all migration files in the given directory, returning the contained Migration.
// If any file operations fail, returns the error.
func (r *Reader) MigrationFromDir(dirPath string) (*Migration, error) {
	paths, err := migrationFilePathsFromDir(dirPath)
	if err != nil {
		return nil, err
	}

	sets := []StatementSet{}
	for _, p := range paths {
		set, err := r.StatementSetFromFile(p)
		if err != nil {
			return nil, err
		}
		sets = append(sets, *set)
	}

	return &Migration{
		StatementSets: sets,
	}, nil
}

// Parses the given file, returning the contained StatementSet.
// If any file operations fail, returns the error.
func (r *Reader) StatementSetFromFile(path string) (*StatementSet, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	name := ""
	statements := []Statement{}
	currLine := -1
	lastStatementLine := 0
	var currStatement *Statement

	appendStatement := func() {
		if currStatement == nil {
			return
		}
		currStatement.EndLine = lastStatementLine
		completedStatement := *currStatement
		statements = append(statements, completedStatement)
		currStatement = nil
	}

	for scanner.Scan() {
		currLine += 1
		line := scanner.Text()

		lineType := lineTypeFor(line)
		switch lineType {

		case lineEmpty:
			continue

		case lineCommentStatementSetName:
			name = statementSetNameFrom(line)

		case lineCommentStatementUpTag, lineCommentStatementDownTag:
			appendStatement()
			var sType string
			if lineType == lineCommentStatementUpTag {
				sType = UpType
			} else {
				sType = DownType
			}
			tag := statementTagFrom(line)
			currStatement = &Statement{
				FilePath: path,
				Type:     sType,
				Tag:      tag,
			}

		case lineStatement:
			if currStatement != nil && currStatement.StartLine == 0 {
				currStatement.StartLine = currLine
			}
			lastStatementLine = currLine

		case lineCommentStatementSetEnd:
			appendStatement()
			break

		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return &StatementSet{
		FilePath:   path,
		Name:       name,
		Statements: statements,
	}, nil
}

func (r *Reader) StatementText(statement *Statement) (string, error) {
	file, err := os.Open(statement.FilePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	currLine := -1
	text := ""
	for scanner.Scan() {
		currLine += 1
		if currLine >= statement.StartLine && currLine <= statement.EndLine {
			text += scanner.Text() + "\n"
		} else if currLine > statement.EndLine {
			break
		}
	}
	text = strings.TrimSpace(text)

	if err := scanner.Err(); err != nil {
		return text, err
	}

	return text, nil
}

// Returns a list of paths to migration files in the given directory.
func migrationFilePathsFromDir(dirPath string) ([]string, error) {
	files, err := os.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}

	// Filter all to only contain files of the migration file type
	paths := []string{}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), migrationFileExt) {
			path := dirPath + "/" + f.Name()
			paths = append(paths, path)
		}
	}

	return paths, nil
}

// Returns the LineType for the given line.
func lineTypeFor(line string) lineType {
	cleanLine := strings.ToLower(strings.TrimSpace(line))

	if cleanLine == "" {
		return lineEmpty
	}

	isComment := strings.HasPrefix(cleanLine, commentPrefix)
	isUpComment := strings.HasPrefix(cleanLine, commentPrefix+" "+UpType)
	isDownComment := strings.HasPrefix(cleanLine, commentPrefix+" "+DownType)
	isEndComment := strings.HasPrefix(cleanLine, commentPrefix+" "+EndTag)
	if isUpComment {
		return lineCommentStatementUpTag
	} else if isDownComment {
		return lineCommentStatementDownTag
	} else if isEndComment {
		return lineCommentStatementSetEnd
	} else if isComment {
		return lineCommentStatementSetName
	}

	return lineStatement
}

// Returns the StatementSet name from a comment describing a StatementSet.
func statementSetNameFrom(line string) string {
	cleanLine := strings.Replace(line, commentPrefix, "", 1)
	components := strings.Split(cleanLine, tagSeparator)
	if len(components) == 0 {
		return ""
	} else {
		return strings.TrimSpace(components[0])
	}
}

// Returns the Statement type and tag from a comment describing a Statement.
func statementTagFrom(line string) string {
	cleanLine := strings.Replace(line, commentPrefix, "", 1)
	components := strings.Split(cleanLine, tagSeparator)
	if len(components) < 2 {
		return ""
	} else {
		return strings.ToLower(strings.TrimSpace(components[1]))
	}
}
