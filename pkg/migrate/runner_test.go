package migrate_test

import (
	"errors"
	"testing"

	"github.com/sirupsen/logrus"
	logrusTest "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/19seanak19/go-simple-migrate/internal/cli"
	"gitlab.com/19seanak19/go-simple-migrate/internal/database/postgres"
	"gitlab.com/19seanak19/go-simple-migrate/pkg/files"
	"gitlab.com/19seanak19/go-simple-migrate/pkg/migrate"
)

const (
	testGoodMigrationsDir = "testdata/test_migrations"
	testSetAPath          = "testdata/test_migrations/set_a.sql"
	testSetBPath          = "testdata/test_migrations/set_b.sql"
)

var testMigration = &files.Migration{
	StatementSets: []files.StatementSet{
		testSetA,
		testSetB,
	},
}

type testReader struct {
	readDir           *string
	readDirErr        error
	readDirMig        *files.Migration
	readFile          []string
	readFileSS        map[string]*files.StatementSet
	readFileErr       error
	readStatements    []files.Statement
	readStatementText string
	readStatementErr  error
}

func (tr *testReader) MigrationFromDir(dirPath string) (*files.Migration, error) {
	tr.readDir = &dirPath
	return tr.readDirMig, tr.readDirErr
}

func (tr *testReader) StatementSetFromFile(path string) (*files.StatementSet, error) {
	tr.readFile = append(tr.readFile, path)
	return tr.readFileSS[path], tr.readFileErr
}

func (tr *testReader) StatementText(statement *files.Statement) (string, error) {
	tr.readStatements = append(tr.readStatements, *statement)
	return tr.readStatementText, tr.readStatementErr
}

func testLogger() (*cli.CliLogger, *logrusTest.Hook) {
	nullLogger, hook := logrusTest.NewNullLogger()
	logger := cli.NewCliLogger()
	logger.Logger = nullLogger
	logger.SetLogLevel(cli.TraceLogLevel)
	return logger, hook
}

type testMigrator struct {
	appliedSS       []string
	appliedTags     []string
	appliedAutoDown bool
	applySSErr      error
}

func (tm *testMigrator) ApplyStatementSet(ss *files.StatementSet, desiredTag string, allowAutoDown bool, dep *migrate.MigratorDependencies) error {
	tm.appliedSS = append(tm.appliedSS, ss.Name)
	tm.appliedTags = append(tm.appliedTags, desiredTag)
	tm.appliedAutoDown = allowAutoDown
	return tm.applySSErr
}

func Test_ApplyMigration_Success(t *testing.T) {
	reader := &testReader{}
	db := &testDb{}
	migrator := &testMigrator{}
	logger, hook := testLogger()
	conf, err := migrate.ConfigFromJson(testGoodConfigPath)
	require.Nil(t, err)
	require.NotNil(t, conf)
	runner := &migrate.Runner{
		Conf:     conf,
		Mig:      testMigration,
		Reader:   reader,
		Db:       db,
		Migrator: migrator,
		Logger:   logger,
	}

	err = runner.ApplyMigration()

	require.Nil(t, err)
	assert.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.InfoLevel, hook.LastEntry().Level)
	assert.Equal(t, 2, len(migrator.appliedTags))
	assert.Equal(t, "set_a", migrator.appliedSS[0])
	assert.Equal(t, "set_b", migrator.appliedSS[1])
	assert.Equal(t, "2", migrator.appliedTags[0])
	assert.Equal(t, "1", migrator.appliedTags[1])
	assert.Equal(t, conf.AllowAutoDown, migrator.appliedAutoDown)
	assert.Equal(t, conf.DbConn, db.openedConn)
	assert.True(t, db.didClose)
}

func Test_ApplyMigration_ErrorDbOpen(t *testing.T) {
	reader := &testReader{}
	db := &testDb{}
	db.openErr = errors.New("Test error")
	migrator := &testMigrator{}
	logger, hook := testLogger()
	conf, err := migrate.ConfigFromJson(testGoodConfigPath)
	require.Nil(t, err)
	require.NotNil(t, conf)
	runner := &migrate.Runner{
		Conf:     conf,
		Mig:      testMigration,
		Reader:   reader,
		Db:       db,
		Migrator: migrator,
		Logger:   logger,
	}

	err = runner.ApplyMigration()

	assert.NotNil(t, err)
	assert.Equal(t, len(hook.Entries), 2)
	assert.Equal(t, hook.LastEntry().Level, logrus.ErrorLevel)
	assert.Equal(t, db.openedConn, conf.DbConn)
	assert.False(t, db.didClose)
}

func Test_ApplyMigration_ErrorApplySS(t *testing.T) {
	reader := &testReader{}
	db := &testDb{}
	migrator := &testMigrator{}
	migrator.applySSErr = errors.New("Test error")
	logger, hook := testLogger()
	conf, err := migrate.ConfigFromJson(testGoodConfigPath)
	require.Nil(t, err)
	require.NotNil(t, conf)
	runner := &migrate.Runner{
		Conf:     conf,
		Mig:      testMigration,
		Reader:   reader,
		Db:       db,
		Migrator: migrator,
		Logger:   logger,
	}

	err = runner.ApplyMigration()

	assert.NotNil(t, err)
	assert.Equal(t, len(hook.Entries), 1)
	assert.Equal(t, db.openedConn, conf.DbConn)
	assert.True(t, db.didClose)
}

func Test_OrderMigrations(t *testing.T) {
	conf, err := migrate.ConfigFromJson(testGoodConfigPath)
	require.Nil(t, err)
	require.NotNil(t, conf)
	runner := &migrate.Runner{
		Conf: conf,
		Mig:  testMigration,
	}

	runner.OrderMigrations()

	assert.Equal(t, runner.Mig.StatementSets[0].Name, "set_b")
	assert.Equal(t, runner.Mig.StatementSets[1].Name, "set_a")
}

func Test_NewRunner_Success(t *testing.T) {
	reader := &testReader{}
	reader.readDirMig = testMigration
	logger, _ := testLogger()
	migrator := &testMigrator{}
	conf, err := migrate.ConfigFromJson(testGoodConfigPath)
	require.Nil(t, err)
	require.NotNil(t, conf)

	runner, err := migrate.NewRunner(conf, reader, migrator, logger)

	require.Nil(t, err)
	require.NotNil(t, runner)
	assert.Equal(t, conf, runner.Conf)
	assert.Equal(t, runner.Mig, testMigration)
	assert.Equal(t, runner.Reader, reader)
	assert.IsType(t, runner.Db, &postgres.Postgres{})
	assert.Equal(t, runner.Migrator, migrator)
	assert.Equal(t, runner.Logger, logger)
}

func Test_NewRunner_ErrorReadMig(t *testing.T) {
	reader := &testReader{}
	reader.readDirErr = errors.New("Test error")
	conf, err := migrate.ConfigFromJson(testGoodConfigPath)
	require.Nil(t, err)
	require.NotNil(t, conf)

	_, err = migrate.NewRunner(conf, reader, migrate.DefaultMigrator, migrate.DefaultLogger)

	require.NotNil(t, err)
}

func Test_NewRunner_ErrorNoDb(t *testing.T) {
	reader := &testReader{}
	reader.readDirMig = testMigration
	conf, err := migrate.ConfigFromJson(testGoodConfigPath)
	conf.DbType = "unknown"
	require.Nil(t, err)
	require.NotNil(t, conf)

	_, err = migrate.NewRunner(conf, reader, migrate.DefaultMigrator, migrate.DefaultLogger)

	require.NotNil(t, err)
}

func Test_NewDefaultRunner_Success(t *testing.T) {
	reader := &testReader{}
	reader.readDirMig = testMigration
	migrate.DefaultReader = reader
	logger, _ := testLogger()
	migrate.DefaultLogger = logger
	migrator := &testMigrator{}
	migrate.DefaultMigrator = migrator
	conf, err := migrate.ConfigFromJson(testGoodConfigPath)
	require.Nil(t, err)
	require.NotNil(t, conf)

	runner, err := migrate.NewDefaultRunner(conf)

	require.Nil(t, err)
	require.NotNil(t, runner)
	assert.Equal(t, conf, runner.Conf)
	assert.Equal(t, runner.Mig, testMigration)
	assert.Equal(t, runner.Reader, reader)
	assert.IsType(t, runner.Db, &postgres.Postgres{})
	assert.Equal(t, runner.Migrator, migrator)
	assert.Equal(t, runner.Logger, logger)
}
