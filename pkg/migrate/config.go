package migrate

import (
	"encoding/json"
	"os"
)

// The config used by the migration runner.
type Config struct {
	// Type of the database (i.e. postgres, mysql, etc.), see docs for options.
	DbType string `json:"dbType"`
	// Connection string to use when connecting to the database.
	DbConn string `json:"dbConn"`
	// Directory where the migration files are stored.
	MigrationsDir string `json:"migrationsDir"`
	// Order in which to apply the migration sets. Empty if not provided.
	Order []string `json:"order"`
	// Mapping of migration set name's to desired tags.
	Tags map[string]*string `json:"desiredTags"`
	// Whether or not the tool can automatically run downs if the deployed tag is newer than the desired.
	AllowAutoDown bool `json:"allowAutoDown"`
}

// Parses the file at the given pass into a Config.
// Returns any errors from reading the file.
func ConfigFromJson(path string) (*Config, error) {
	contents, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var conf Config
	err = json.Unmarshal(contents, &conf)
	if err != nil {
		return nil, err
	}

	return &conf, nil
}
