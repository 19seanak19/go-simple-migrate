package migrate

// Interface definition for the logger
type Logger interface {
	Trace(msg string)
	Debug(msg string)
	Info(msg string)
	Warn(msg string)
	Error(msg string)
}

// A default Logger that prints no messages.
type EmptyLogger struct {
}

// Does nothing.
func (el *EmptyLogger) Trace(msg string) {

}

// Does nothing.
func (el *EmptyLogger) Debug(msg string) {

}

// Does nothing.
func (el *EmptyLogger) Info(msg string) {

}

// Does nothing.
func (el *EmptyLogger) Warn(msg string) {

}

// Does nothing.
func (el *EmptyLogger) Error(msg string) {

}
