package migrate

import (
	"errors"
	"sort"

	"gitlab.com/19seanak19/go-simple-migrate/pkg/database"
	"gitlab.com/19seanak19/go-simple-migrate/pkg/files"
)

// Default ReaderInterface that should generally be used when creating a new Runner.
var DefaultReader files.ReaderInterface = &files.Reader{}

// Default Logger interface that should be used when creating a Runner that should do no logging.
var DefaultLogger Logger = &EmptyLogger{}

// Default MigratorInterface that should be used when creating a new Runner.
var DefaultMigrator MigratorInterface = &Migrator{}

var ErrorNoDbInterface = errors.New("No database interface exists for given database type")

// Interface definition for types that orchestrate the running of migrations.
type RunnerInterface interface {
	ApplyMigration() error
	OrderMigrations()
}

// The type responsible for orchestrating migrations.
type Runner struct {
	// The config used when running the migration.
	Conf *Config
	// Reader used by the runner
	Reader files.ReaderInterface
	// The migration for this runner to run.
	Mig *files.Migration
	// The interface for interacting with the database.
	Db database.DatabaseInterface
	// The interface for applying migration files.
	Migrator MigratorInterface
	// The logger used when running migrations.
	Logger Logger
}

// Applies the migrations as configured in the Migrations's Conf.
// Returns any errors encountered when interacting with the DB.
func (r *Runner) ApplyMigration() error {
	r.Logger.Info("Applying migrations...")

	err := r.Db.Open(r.Conf.DbConn)
	if err != nil {
		r.Logger.Error("Failed to open connection to DB")
		return err
	}
	defer r.Db.Close()

	deps := &MigratorDependencies{
		Reader: r.Reader,
		Db:     r.Db,
		Logger: r.Logger,
	}

	for _, ss := range r.Mig.StatementSets {
		var desiredTag string
		confTag := r.Conf.Tags[ss.Name]
		if confTag != nil {
			desiredTag = *confTag
		} else {
			desiredTag = ss.Statements[len(ss.Statements)-1].Tag
		}

		err := r.Migrator.ApplyStatementSet(&ss, desiredTag, r.Conf.AllowAutoDown, deps)
		if err != nil {
			return err
		}
	}

	return nil
}

// Orders the runner's migrations, first based on the config, then alphabetically.
func (r *Runner) OrderMigrations() {
	confOrder := map[string]*int{}
	for i, name := range r.Conf.Order {
		confOrder[name] = &i
	}

	sort.Slice(r.Mig.StatementSets, func(i, j int) bool {
		iName := r.Mig.StatementSets[i].Name
		jName := r.Mig.StatementSets[j].Name

		iConfIndex := confOrder[iName]
		jConfIndex := confOrder[jName]

		if iConfIndex != nil && jConfIndex != nil {
			return *iConfIndex < *jConfIndex
		} else if iConfIndex == nil && jConfIndex == nil {
			return iName < jName
		} else {
			return iConfIndex != nil
		}
	})
}

// Creates a new Runner.
// Migrations are read based on the config, and the order is updated to match the config.
//
// If not creating a custom reader or logger, use migrate.DefaultReader and migrate.DefaultLogger
// for these parameters.
//
// Returns the error from reading migration files if one occurs.
func NewRunner(conf *Config, reader files.ReaderInterface, migrator MigratorInterface, logger Logger) (*Runner, error) {
	mig, err := reader.MigrationFromDir(conf.MigrationsDir)
	if err != nil {
		return nil, err
	}

	db := database.InterfaceForId(conf.DbType)
	if db == nil {
		return nil, ErrorNoDbInterface
	}

	runner := &Runner{
		Conf:     conf,
		Mig:      mig,
		Reader:   reader,
		Db:       db,
		Migrator: migrator,
		Logger:   logger,
	}

	runner.OrderMigrations()

	return runner, nil
}

// Creates a new Runner using the default reader, migrator, and logger.
// Functions the same as NewRunner.
func NewDefaultRunner(conf *Config) (*Runner, error) {
	return NewRunner(conf, DefaultReader, DefaultMigrator, DefaultLogger)
}
