package migrate_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/19seanak19/go-simple-migrate/pkg/migrate"
)

const (
	testGoodConfigPath      = "testdata/test_config.json"
	testNoConfigPath        = "testdata/nonexistent_config.json"
	testMalformedConfigPath = "testdata/test_config_bad.json"
)

func Test_ConfigFromJson_Success(t *testing.T) {
	tag := "1"
	expected := &migrate.Config{
		DbType:        "postgres",
		DbConn:        "postgesql//user:pass@localhost:5432/database",
		MigrationsDir: "testdata/test_migrations",
		Order:         []string{"set_b"},
		Tags:          map[string]*string{"set_b": &tag},
		AllowAutoDown: true,
	}

	conf, err := migrate.ConfigFromJson(testGoodConfigPath)

	require.Nil(t, err)
	assert.Equal(t, expected, conf)
}

func Test_ConfigFromJson_ErrorNotFound(t *testing.T) {
	_, err := migrate.ConfigFromJson(testNoConfigPath)
	if err == nil {
		t.Errorf("Did not return error for non-existant config file")
	}
}

func Test_ConfigFromJson_ErrorMalformed(t *testing.T) {
	_, err := migrate.ConfigFromJson(testMalformedConfigPath)

	assert.NotNil(t, err)
}
