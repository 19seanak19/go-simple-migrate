package migrate_test

import (
	"errors"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/19seanak19/go-simple-migrate/pkg/files"
	"gitlab.com/19seanak19/go-simple-migrate/pkg/migrate"
)

var testSetA = files.StatementSet{
	FilePath: testSetAPath,
	Name:     "set_a",
	Statements: []files.Statement{
		{
			FilePath:  testSetAPath,
			Type:      files.UpType,
			Tag:       "1",
			StartLine: 4,
			EndLine:   6,
		},
		{
			FilePath:  testSetAPath,
			Type:      files.DownType,
			Tag:       "1",
			StartLine: 10,
			EndLine:   11,
		},
		{
			FilePath:  testSetAPath,
			Type:      files.UpType,
			Tag:       "2",
			StartLine: 15,
			EndLine:   16,
		},
		{
			FilePath:  testSetAPath,
			Type:      files.DownType,
			Tag:       "2",
			StartLine: 20,
			EndLine:   21,
		},
	},
}

var testSetB = files.StatementSet{
	FilePath: testSetBPath,
	Name:     "set_b",
	Statements: []files.Statement{
		{
			FilePath:  testSetBPath,
			Type:      files.UpType,
			Tag:       "1",
			StartLine: 4,
			EndLine:   6,
		},
		{
			FilePath:  testSetBPath,
			Type:      files.DownType,
			Tag:       "1",
			StartLine: 10,
			EndLine:   11,
		},
		{
			FilePath:  testSetBPath,
			Type:      files.UpType,
			Tag:       "2",
			StartLine: 15,
			EndLine:   16,
		},
		{
			FilePath:  testSetBPath,
			Type:      files.DownType,
			Tag:       "2",
			StartLine: 20,
			EndLine:   21,
		},
	},
}

type testDb struct {
	openedConn     string
	openErr        error
	didClose       bool
	closeErr       error
	appliedTagErr  error
	preAppliedTags map[string]string
	appliedSetName string
	appliedTags    []string
	applyError     error
}

func (tdb *testDb) Open(conn string) error {
	tdb.openedConn = conn
	return tdb.openErr
}

func (tdb *testDb) Close() error {
	tdb.didClose = true
	return tdb.closeErr
}

func (tdb *testDb) AppliedTagForSet(setName string) (string, error) {
	return tdb.preAppliedTags[setName], tdb.appliedTagErr
}

func (tdb *testDb) ApplyMigration(statement string, tag string, setName string) error {
	tdb.appliedSetName = setName
	tdb.appliedTags = append(tdb.appliedTags, tag)
	return tdb.applyError
}

func Test_ApplyStatementSet_SuccessUps(t *testing.T) {
	reader := &testReader{}
	db := &testDb{}
	logger, hook := testLogger()
	migrator := &migrate.Migrator{}
	deps := &migrate.MigratorDependencies{
		Reader: reader,
		Db:     db,
		Logger: logger,
	}

	err := migrator.ApplyStatementSet(&testSetA, "", false, deps)

	require.Nil(t, err)
	assert.Equal(t, 2, len(hook.Entries))
	assert.Equal(t, testSetA.Name, db.appliedSetName)
	assert.Equal(t, []string{"1", "2"}, db.appliedTags)
}

func Test_ApplyStatementSet_SuccessDownsAllowed(t *testing.T) {
	reader := &testReader{}
	db := &testDb{}
	db.preAppliedTags = map[string]string{testSetA.Name: "2"}
	logger, hook := testLogger()
	migrator := &migrate.Migrator{}
	deps := &migrate.MigratorDependencies{
		Reader: reader,
		Db:     db,
		Logger: logger,
	}

	err := migrator.ApplyStatementSet(&testSetA, "", true, deps)

	require.Nil(t, err)
	assert.Equal(t, 2, len(hook.Entries))
	assert.Equal(t, []string{"2", "1"}, db.appliedTags)
}

func Test_ApplyStatementSet_SuccessDownsNotAllowed(t *testing.T) {
	reader := &testReader{}
	db := &testDb{}
	db.preAppliedTags = map[string]string{testSetA.Name: "2"}
	logger, hook := testLogger()
	migrator := &migrate.Migrator{}
	deps := &migrate.MigratorDependencies{
		Reader: reader,
		Db:     db,
		Logger: logger,
	}

	err := migrator.ApplyStatementSet(&testSetA, "", false, deps)

	require.Nil(t, err)
	assert.Equal(t, 2, len(hook.Entries))
	assert.Equal(t, logrus.WarnLevel, hook.Entries[1].Level)
}

func Test_ApplyStatementSet_ErrorAppliedTag(t *testing.T) {
	reader := &testReader{}
	db := &testDb{}
	db.appliedTagErr = errors.New("test error")
	logger, hook := testLogger()
	migrator := &migrate.Migrator{}
	deps := &migrate.MigratorDependencies{
		Reader: reader,
		Db:     db,
		Logger: logger,
	}

	err := migrator.ApplyStatementSet(&testSetA, "", false, deps)

	require.NotNil(t, err)
	assert.Equal(t, 1, len(hook.Entries))
}

func Test_ApplyStatementSet_ErrorUpsRead(t *testing.T) {
	reader := &testReader{}
	reader.readStatementErr = errors.New("Test error")
	db := &testDb{}
	logger, hook := testLogger()
	migrator := &migrate.Migrator{}
	deps := &migrate.MigratorDependencies{
		Reader: reader,
		Db:     db,
		Logger: logger,
	}

	err := migrator.ApplyStatementSet(&testSetA, "", false, deps)

	require.NotNil(t, err)
	assert.Equal(t, 3, len(hook.Entries))
	assert.Equal(t, logrus.ErrorLevel, hook.Entries[2].Level)
}

func Test_ApplyStatementSet_ErrorUpsApply(t *testing.T) {
	reader := &testReader{}
	db := &testDb{}
	db.applyError = errors.New("Test error")
	logger, hook := testLogger()
	migrator := &migrate.Migrator{}
	deps := &migrate.MigratorDependencies{
		Reader: reader,
		Db:     db,
		Logger: logger,
	}

	err := migrator.ApplyStatementSet(&testSetA, "", false, deps)

	require.NotNil(t, err)
	assert.Equal(t, 2, len(hook.Entries))
}

func Test_ApplyStatementSet_ErrorDownsRead(t *testing.T) {
	reader := &testReader{}
	reader.readStatementErr = errors.New("Test error")
	db := &testDb{}
	db.preAppliedTags = map[string]string{testSetA.Name: "2"}
	logger, hook := testLogger()
	migrator := &migrate.Migrator{}
	deps := &migrate.MigratorDependencies{
		Reader: reader,
		Db:     db,
		Logger: logger,
	}

	err := migrator.ApplyStatementSet(&testSetA, "", true, deps)

	require.NotNil(t, err)
	assert.Equal(t, 3, len(hook.Entries))
	assert.Equal(t, logrus.ErrorLevel, hook.Entries[2].Level)
}

func Test_ApplyStatementSet_ErrorDownsApply(t *testing.T) {
	reader := &testReader{}
	db := &testDb{}
	db.preAppliedTags = map[string]string{testSetA.Name: "2"}
	db.applyError = errors.New("Test error")
	logger, hook := testLogger()
	migrator := &migrate.Migrator{}
	deps := &migrate.MigratorDependencies{
		Reader: reader,
		Db:     db,
		Logger: logger,
	}

	err := migrator.ApplyStatementSet(&testSetA, "", true, deps)

	require.NotNil(t, err)
	assert.Equal(t, 2, len(hook.Entries))
}
