// Package migrate implements functions and types for actually applying migrations.
//
// The basic usage is to create a Runner, then use that Runner to apply the migrations.
package migrate

import (
	"fmt"

	"gitlab.com/19seanak19/go-simple-migrate/pkg/database"
	"gitlab.com/19seanak19/go-simple-migrate/pkg/files"
)

// Interface definition for types that can apply individual migration files.
type MigratorInterface interface {
	ApplyStatementSet(ss *files.StatementSet, desiredTag string, allowAutoDown bool, dep *MigratorDependencies) error
}

// Dependencies that are used by Migrator when applying migration files.
type MigratorDependencies struct {
	Reader files.ReaderInterface
	Db     database.DatabaseInterface
	Logger Logger
}

// Type for applying individual migration files.
type Migrator struct {
}

// Applies the Statements in the given StatementSet.
// deps.Db should already have an open connection.
// Returns any errors encountered when interacting with the DB.
func (m *Migrator) ApplyStatementSet(ss *files.StatementSet, desiredTag string, allowAutoDown bool, deps *MigratorDependencies) error {
	deps.Logger.Info(fmt.Sprintf("Applying Statement Set %s", ss.Name))

	tagOrder := ss.UpTagOrder()

	appliedTag, err := deps.Db.AppliedTagForSet(ss.Name)
	if err != nil {
		return err
	}

	emptyTagOrder := -1

	aOrder := tagOrder[appliedTag]
	if aOrder == nil {
		aOrder = &emptyTagOrder
	}

	dOrder := tagOrder[desiredTag]
	if desiredTag == "" {
		dOrder = &emptyTagOrder
	}

	if appliedTag == "" || (*aOrder < *dOrder) {
		deps.Logger.Info(fmt.Sprintf("Applying Up statements from %s to %s", appliedTag, desiredTag))
		err := m.applyUps(ss, appliedTag, desiredTag, deps)
		if err != nil {
			return err
		}
	} else if allowAutoDown {
		deps.Logger.Info(fmt.Sprintf("Applying Down statements from %s to %s", appliedTag, desiredTag))
		err := m.applyDowns(ss, appliedTag, desiredTag, deps)
		if err != nil {
			return err
		}
	} else {
		deps.Logger.Warn("AllowAutoDowns is false, skipping migration requiring Downs")
	}

	return nil
}

// Applies the Up statements in the set, starting from the tag after the applied and ending with the desiredTag.
// deps.Db should already have an open connection.
// Returns any errors encountered when interacting with the DB.
func (m *Migrator) applyUps(ss *files.StatementSet, appliedTag string, desiredTag string, deps *MigratorDependencies) error {
	apply := false
	for _, s := range ss.Statements {
		if !apply && (appliedTag == "" || appliedTag == s.Tag) {
			apply = true
			if appliedTag != "" {
				continue
			}
		}

		if !apply || s.Type != files.UpType {
			continue
		}

		sql, err := deps.Reader.StatementText(&s)
		if err != nil {
			deps.Logger.Error(fmt.Sprintf("Failed to read statement text for %s", s.Tag))
			return err
		}

		err = deps.Db.ApplyMigration(sql, s.Tag, ss.Name)
		if err != nil {
			return err
		}

		if s.Tag == desiredTag {
			break
		}
	}

	return nil
}

// Applies the Down statements in the set, starting from the appliedTag and ending with the desiredTag.
// deps.Db should already have an open connection.
// Returns any errors encountered when interacting with the DB.
func (m *Migrator) applyDowns(ss *files.StatementSet, appliedTag string, desiredTag string, deps *MigratorDependencies) error {
	apply := false
	for i := len(ss.Statements) - 1; i >= 0; i-- {
		s := ss.Statements[i]
		if !apply && appliedTag == s.Tag {
			apply = true
		}

		if !apply || s.Type != files.DownType {
			continue
		}

		sql, err := deps.Reader.StatementText(&s)
		if err != nil {
			deps.Logger.Error(fmt.Sprintf("Failed to read statement text for %s", s.Tag))
			return err
		}

		var newTag string
		if i != 0 {
			newTag = ss.Statements[i-1].Tag
		}

		err = deps.Db.ApplyMigration(sql, newTag, ss.Name)
		if err != nil {
			return err
		}

		if newTag == desiredTag {
			break
		}
	}

	return nil
}
