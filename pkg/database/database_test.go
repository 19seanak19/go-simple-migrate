package database_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/19seanak19/go-simple-migrate/internal/database/postgres"
	"gitlab.com/19seanak19/go-simple-migrate/pkg/database"
)

func Test_InterfaceForId_Nil(t *testing.T) {
	db := database.InterfaceForId("unknown")

	assert.Nil(t, db)
}

func Test_InterfaceForId_Postgres(t *testing.T) {
	db := database.InterfaceForId(database.IdPostgres)

	require.NotNil(t, db)
	assert.IsType(t, db, &postgres.Postgres{})
}
