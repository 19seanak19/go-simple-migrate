// Package database defines the DatabaseInterface interface for communicating with databases along with utilities for
// using internally defined interfaces.
//
// In general, this package should not be used directly.
package database

import (
	"gitlab.com/19seanak19/go-simple-migrate/internal/database/postgres"
)

const (
	// Id for the Postgres interface.
	IdPostgres = "postgres"
)

// Interface for types that can handle connecting to a database
// and applying migrations.
type DatabaseInterface interface {
	// Opens a connection to the DB using the given connection string.
	Open(conn string) error
	// Closes any open connection to the DB.
	Close() error
	// Returns the currently applied tag for the set with the given name.
	AppliedTagForSet(setName string) (string, error)
	// Applies the given statement, updating the applied tag for the set name.
	// The tag for Ups should be the statement's tag.
	// The tag for Downs should be the previous statement's tag.
	ApplyMigration(statement string, tag string, setName string) error
}

// Returns the interface matching the given id.
func InterfaceForId(id string) DatabaseInterface {
	switch id {
	case IdPostgres:
		return &postgres.Postgres{}
	}
	return nil
}
